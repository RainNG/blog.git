<div align='center' ><font size='8'>Blog</font></div>

:smiley: [我的博客主页](https://rainng.gitee.io/blog) :smiley:



**目录**

[TOC]

修订记录：

| 版本号 | 作者     | 修改日期   | 修改说明               |
| ------ | -------- | ---------- | ---------------------- |
| V1.0.0 | 夜色微凉 | 2021/12/20 | 提交hexo init初版      |
| V1.0.0 | 夜色微凉 | 2021/12/21 | 更换主题为 gal         |
| V1.0.0 | 夜色微凉 | 2021/12/22 | 添加文章“blog搭建教程” |
| V1.0.0 | 夜色微凉 | 2021/12/23 | 添加文章“git教程”      |





# 介绍
​	使用Hexo搭建个人博客，Hexo是一个基于 nodejs 的快速生成静态博客的开源框架。

​	搭建过程参考 **[blog搭建教程](https://rainng.gitee.io/blog/2021/12/22/blog%E6%90%AD%E5%BB%BA%E6%95%99%E7%A8%8B/)**

## 分支说明

master分支：blog源码分支。

pages分支：blog Pages服务分支。

# Blog文章

# 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


# 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
